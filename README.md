# Bike API

Bike Client is React crud application created with [Create React App][create-react-app]. It is written on [Typescript][Typescript] and use [React framework][react]


## Tech Stack 

* [React][react], [Redux][redux], [Redux saga][redux-saga],[Typescript][typescript], [Lambda TS][lambda-ts], [Yarn][yarn]

## Prerequisites

* [Docker][docker] Community Edition v20 or higher

## Getting Started

Clone the repository

```
git@gitlab.com:ayovcheff/bike-app.git
```

Build and run the project

```
cd bike-client
docker-compose up
```

To reach out the API navigate to `http://127.0.0.1:3000`


[react]: https://reactjs.org
[typescript]: https://github.com/kriasoft/
[redux]: https://redux.js.org
[redux-saga]: https://redux-saga.js.org
[create-react-app]: https://create-react-app.dev
[lambda-ts]: https://github.com/aYo-dev/lambda-ts
[yarn]: https://yarnpkg.com/
[docker]: https://www.docker.com/community-edition
