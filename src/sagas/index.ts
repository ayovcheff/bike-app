import { all } from 'redux-saga/effects';
import { applicationSaga } from './application.saga';
import { notificationSaga } from './notification.saga';
import { productSaga } from './product.saga';
import { userSaga } from './user.saga';

export default function* rootSaga() {
  yield all([
    applicationSaga(),
    productSaga(),
    userSaga(),
    notificationSaga(),
  ]);
}