import {put, takeLatest } from 'redux-saga/effects';

import { applicationLoadActions } from '../constants/actions/application.constants';
import { IAction } from '../interfaces/action.interface';
import { extractUserFromToken } from './user.saga';

export const loading = (data: any) => applicationLoadActions.request(data);

function* loadApplication(data: IAction): any {
  try {
    yield put(extractUserFromToken(data.payload.onLoadingFail));
    yield put(applicationLoadActions.success());
  } catch (e) {
    yield put(applicationLoadActions.failure(e));
  }
}

export function* applicationSaga() {
  yield takeLatest(applicationLoadActions.request().type, loadApplication);
}