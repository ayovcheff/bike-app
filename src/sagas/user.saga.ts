import {put, takeLatest, call, take } from 'redux-saga/effects'
import { Either, TryCatch } from 'lambda-ts';

import {
  fetchUserActions,
  createUserActions,
  extractUser,
  loginActions,
  logoutActions,
} from '../constants/actions/user.constants';
import { IAction } from '../interfaces/action.interface';
import { ILoginFormInput, IUserCreationData } from '../interfaces/user.interface';
import { api } from '../services/api.service';
import { extractDataFromToken } from '../services/app.service';
import { NOTIFY_ERROR } from '../constants/actions/notification.constants';
import { applicationLoadActions } from '../constants/actions';

export const registration = (data: IUserCreationData) => createUserActions.request(data);
export const signIn = (data: ILoginFormInput) => loginActions.request(data);
export const signOut = () => logoutActions.request();
export const extractUserFromToken = (onLoadingFail: any) => extractUser.request({onLoadingFail});

function* login(action: IAction): any {
  try {
    const result = yield call(api.post, '/login', action.payload);
    
    const tokenMonad = TryCatch(() => result.json.token);
    
    // check if token exist
    if(tokenMonad.isLeft()) {
      throw new Error('Login unssuccessful. Please contact support.');
    }

    sessionStorage.setItem('token', tokenMonad.getOrElse(undefined));
    const userData = extractDataFromToken(tokenMonad)

    yield put(loginActions.success(userData));
  } catch (e) {
    yield put(loginActions.failure(e));
    yield put({type: NOTIFY_ERROR, payload: {
      message: 'Wrong credentials',
    }});
  }
}

function* logout(action: IAction): any {
  try {
    sessionStorage.removeItem('token');
    yield put(logoutActions.success());
  } catch (e) {
    yield put(logoutActions.failure(e));
  }
}

// worker Saga: will be fired on USER_FETCH_REQUESTED actions
function* fetchUser(): any {
  try {
    const user = yield call(api.get, '/');
    yield put(fetchUserActions.success(user));
  } catch (e) {
    yield put(fetchUserActions.failure(e));
  }
}

function* createUser(action: IAction): any {
  try {
    yield call(api.post, '/user', action.payload);
    yield put(createUserActions.success());
  } catch (e) {
    yield put(createUserActions.failure(e));
  }
}

function* extractUserData(data: IAction): any {
  try{
    // wait until app is loaded
    yield take(applicationLoadActions.success().type);
    
    const maybeToken = TryCatch(() => 
      sessionStorage.getItem('token'))
      .chain(v => Either(v));

    if(maybeToken.isLeft()) {
      throw new Error('Token not found');
    }

    // check if token is expired
    yield call(api.get, '/token', true);

    const userData = extractDataFromToken(maybeToken);

    yield put(extractUser.success(userData));
  } catch(e) {
    yield put(extractUser.failure(e));
  }
}

export function* userSaga() {
  yield takeLatest(loginActions.request().type, login);
  yield takeLatest(logoutActions.request().type, logout);
  yield takeLatest(extractUser.request().type, extractUserData);
  yield takeLatest(fetchUserActions.request().type, fetchUser);
  yield takeLatest(createUserActions.request().type, createUser);
}