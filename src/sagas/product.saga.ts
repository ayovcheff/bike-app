import { omit } from 'ramda';
import {put, takeLatest, call } from 'redux-saga/effects'

import {
  fetchProductActions,
  deleteProductActions,
  createProductActions,
  fetchAllProductsActions,
  resetProductActions,
  editProductActions,
} from '../constants/actions';
import { IAction } from '../interfaces/action.interface';
import { IProductFormData } from '../interfaces/product.interface';
import { api } from '../services/api.service';

export const createProduct = (data: IProductFormData) => createProductActions.request(data);
export const fetchAllProducts = () => fetchAllProductsActions.request();
export const deleteProduct = (productId: string) => deleteProductActions.request({productId});
export const fetchProduct = (productId: string) => fetchProductActions.request({productId});
export const resetProduct = () => resetProductActions.success();
export const editProduct = (data: IProductFormData, productId: string) => editProductActions.request({data, productId});

function* create(action: IAction): any {
  try {
    const productData = omit(['onSuccess'], action.payload);

    yield call(api.post, '/product', productData, true);
    yield put(createProductActions.success());
    action.payload.onSuccess();
  } catch (e) {
    yield put(createProductActions.failure(e));
  }
}

function* fetch(action: IAction): any {
  try {
    const result = yield call(api.get, `/product/${action.payload.productId}`, true);
    yield put(fetchProductActions.success(result.json.data));
  } catch (e) {
    yield put(fetchProductActions.failure(e));
  }
}

function* fetchAll(): any {
  try {
    const result = yield call(api.get, '/products', true);
    console.log('all products result', result);
    yield put(fetchAllProductsActions.success(result.json.data));
  } catch (e) {
    yield put(fetchAllProductsActions.failure(e));
  }
}

function* remove(action: IAction): any {
  try {
    yield call(api.post, `/product/${action.payload.productId}/delete`, {}, true);
    yield put(deleteProductActions.success(action.payload));
  } catch (e) {
    yield put(deleteProductActions.failure(e));
  }
}

function* edit(action: IAction): any {
  try {
    const productData = omit(['onSuccess'], action.payload.data);

    yield call(api.post, `/product/${action.payload.productId}/edit`, productData, true);
    action.payload.data.onSuccess();
    yield put(editProductActions.success(action.payload.data));
  } catch (e) {
    yield put(editProductActions.failure(e));
  }
}

export function* productSaga() {
  yield takeLatest(fetchAllProductsActions.request().type, fetchAll);
  yield takeLatest(fetchProductActions.request().type, fetch);
  yield takeLatest(deleteProductActions.request().type, remove);
  yield takeLatest(createProductActions.request().type, create);
  yield takeLatest(editProductActions.request().type, edit);
}