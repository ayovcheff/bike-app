import { equals } from "ramda";
import { put, takeEvery } from "redux-saga/effects";
import { v4 as uuidv4 } from 'uuid';

import { ADD_NOTIFICATION, NOTIFY_ERROR, NOTIFY_INFO, NOTIFY_SUCCESS, NOTIFY_WARNING, REMOVE_NOTIFICATION } from "../constants/actions/notification.constants";
import { NotificationTypes } from "../enums";
import { IAction } from "../interfaces/action.interface";

const addNotificationType = (actionType: string): NotificationTypes => {
  if (equals(NOTIFY_ERROR, actionType)) {
    return NotificationTypes.error;
  }

  if (equals(NOTIFY_WARNING, actionType)) {
    return NotificationTypes.warning;
  }

  if (equals(NOTIFY_SUCCESS, actionType)) {
    return NotificationTypes.success;
  }

  return NotificationTypes.info;
}

export const removeNotification = (id: string) => ({
  type: REMOVE_NOTIFICATION,
  payload: { id }
});

function* addNotification(action: IAction) {
  yield put({
    type: ADD_NOTIFICATION,
    payload: { ...action.payload, id: uuidv4(), type: addNotificationType(action.type) },
  })
}

export function* notificationSaga() {
  yield takeEvery([NOTIFY_INFO, NOTIFY_SUCCESS, NOTIFY_WARNING, NOTIFY_ERROR], addNotification);
}