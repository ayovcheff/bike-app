import { isNil } from "ramda";
import { useEffect, useState } from "react";

const useToken = (defaultValue: any) => {
  const [token, setToken] = useState(defaultValue);

  useEffect(() => {
    if(isNil(token)) {
      const result = sessionStorage.getItem('token');
      setToken(result);
    }
  }, [token]);

  return token;
}

export default useToken;