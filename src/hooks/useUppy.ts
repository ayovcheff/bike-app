import { useEffect, useState } from 'react';

import Uppy from '@uppy/core';
import XHRUpload from '@uppy/xhr-upload';
import { isNil } from 'ramda';

import useToken from './useToken';
import { useUppy } from '@uppy/react';

type feedBackFN = (value: string) => void;
const maxFileSize = 1000000 * 3;

const useUppyUpload = (endpoint: string, onSuccess: feedBackFN, onFailure: feedBackFN) => {
  const token = useToken(null);
  const [isSuccessSet, setSuccess] = useState(false);
  const [isFailureSet, setFailure] = useState(false);

  const uppy = useUppy(() => {
    return new Uppy({
      meta: { type: 'image' },
      autoProceed: true,
      restrictions: {
        maxFileSize,
        maxNumberOfFiles: 5,
        allowedFileTypes: ['image/*', '.jpg', '.jpeg', '.png'],
      }
    });
  })

  useEffect(() => {
    if(!isNil(token)) {
      uppy.use(XHRUpload, {
        endpoint: `${process.env.REACT_APP_API_URL}/${endpoint}`,
        headers: {
          authorization: `Bearer ${token}`,
        },
      });
    }
  }, [uppy, token, endpoint]);

  useEffect(() => {
    if(isSuccessSet) return;
    setSuccess(true);

    uppy.on('upload-success', (_, response) => {
      const uploadURL = response?.body?.data;

      onSuccess(uploadURL);
    });
  }, [uppy, onSuccess, isSuccessSet]);

  useEffect(() => {
    if(isFailureSet) return;
    setFailure(true);

    uppy.on('upload-error', (_, error) => {
      return onFailure(error.message);
    });
  }, [uppy, onFailure, isFailureSet]);

  return uppy;
}

export default useUppyUpload;