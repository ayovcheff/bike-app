import jwtDecode from "jwt-decode";
import { Monad } from "lambda-ts";
import { pick } from "ramda";
import { IUserState } from "../interfaces/user.interface";

export const extractDataFromToken = (maybeToken: Monad<string | null>): IUserState =>
  maybeToken
    .map((v) => jwtDecode(v as string))
    .map(pick(['email', 'id', 'name']))
    .get() as IUserState;