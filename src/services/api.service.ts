import { concat } from 'ramda';
import { Either } from 'lambda-ts';
import { IResponse } from '../interfaces/response.interface';

const API_URL = process.env.REACT_APP_API_URL as string;

const validateResponse = (response: Response) => {
  if (response.status >= 200 && response.status < 400) {
    return response;
  }

  // unauthorised
  if (response.status === 401) {
    // dispatch(authActions.default.logoutRequest());
    throw new Error('user is not authorized');
  }

  throw new Error('An error occured when calling the API');
}

const normalizeResponseData = (response: Response) => {
  const headers = response.headers;
  return response.json().then(json => ({ json, headers }));
}

// const createUrl = concat(API_URL);
const createUrl = Either(API_URL);

const getMethodOptions = (isPrivate: boolean, defaultOptions: Record<string, any>, options: Record<string, any>) => 
  Either(isPrivate)
    .map(() => sessionStorage.getItem('token'))
    .map(token => ({
      ...defaultOptions,
      ...options,
      headers: {
        ...defaultOptions.headers,
        'Authorization': `Bearer ${token}`,
      },
    }));

const get = async (url: string, isPrivate: boolean = false, options: Record<string, string> = {}): Promise<IResponse> => {
  const defaultOptions = {
    headers: {
      'Content-Type': 'application/json',
    },
    method: 'get',
  };

  const finalOptions = getMethodOptions(isPrivate, defaultOptions, options)
    .getOrElse(defaultOptions) as RequestInit;

  const maybeURL = createUrl.map((v) => concat(v, url));

  if(maybeURL.isLeft()) {
    throw new Error('There is an error with your url!');
  }

  return fetch(maybeURL.get(), finalOptions)
    .then(validateResponse)
    .then(normalizeResponseData)
    .catch((error) => {
      throw error;
    });
};

const post = async <T>(url: string, data: T, isPrivate: boolean = false, options: Record<string, string> = {}): Promise<IResponse> => {
  const defaultOptions = {
    headers: {
      'Content-Type': 'application/json',
    },
    method: 'POST',
    body: JSON.stringify(data),
  };

  const finalOptions = getMethodOptions(isPrivate, defaultOptions, options)
    .getOrElse(defaultOptions) as RequestInit;

  const maybeURL = createUrl.map((v) => concat(v, url));

  if(maybeURL.isLeft()) {
    throw new Error('There is an error with your url!');
  }

  return fetch(maybeURL.get(), finalOptions)
    .then(validateResponse)
    .then(normalizeResponseData)
    .catch((error) => {
      throw error;
    });
};

export const api = {
  get,
  post,
};