import { createActionNames } from "../../utils";

// Actions
const EXTRACT_USER = 'EXTRACT_USER';
const LOGIN = 'LOGIN';
const LOGOUT = 'LOGOUT';
const CREATE_USER = 'CREATE_USER';
const FETCH_USER = 'FETCH_USER';

export const fetchUserActions = createActionNames(FETCH_USER);
export const createUserActions = createActionNames(CREATE_USER);
export const extractUser = createActionNames(EXTRACT_USER);
export const loginActions = createActionNames(LOGIN);
export const logoutActions = createActionNames(LOGOUT);

// Messages
export const REGISTRATION_SUCCESS = 'Congratulations! You have been successfully sign up! Please sign in.'