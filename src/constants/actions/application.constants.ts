import { createActionNames } from "../../utils";

export const APPLICATION_LOAD = 'APPLICATION_LOAD';
export const applicationLoadActions = createActionNames(APPLICATION_LOAD);
