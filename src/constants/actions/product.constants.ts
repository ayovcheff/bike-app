import { createActionNames } from "../../utils";

const FETCH_PRODUCT = 'FETCH_PRODUCT';
const FETCH_ALL_PRODUCTS = 'FETCH_ALL_PRODUCTS';
const CREATE_PRODUCT = 'CREATE_PRODUCT';
const DELETE_PRODUCT = 'DELETE_PRODUCT';
const RESET_PRODUCT = 'RESET_PRODUCT';
const EDIT_PRODUCT = 'EDIT_PRODUCT';

export const fetchAllProductsActions = createActionNames(FETCH_ALL_PRODUCTS);
export const fetchProductActions = createActionNames(FETCH_PRODUCT);
export const createProductActions = createActionNames(CREATE_PRODUCT);
export const deleteProductActions = createActionNames(DELETE_PRODUCT);
export const resetProductActions = createActionNames(RESET_PRODUCT);
export const editProductActions = createActionNames(EDIT_PRODUCT);