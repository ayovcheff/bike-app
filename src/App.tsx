import { useCallback, useEffect, useMemo } from 'react';
import { Routes, Route, useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';

import Snackbar from '@mui/material/Snackbar';
import { purple, red } from '@mui/material/colors';
import { ThemeProvider, createTheme } from '@mui/material/styles';
import { ThemeProvider as StyledThemeProvider } from 'styled-components';
import Box from '@mui/material/Box';
import Stack from '@mui/material/Stack';

import './App.css';
import { RequireAuth } from './containers/RequiredAuth';
import { IApplicationState } from './interfaces/application.interface';
import { isApplicationLoaded } from './selectors/application.selectors';
import { loading } from './sagas/application.saga';
import { Products } from './containers/Products';
import { Product } from './containers/Product';
import { Register } from './containers/Register';
import { Login } from './containers/Login';
import { Header} from './components/Header';

import { removeNotification } from './sagas/notification.saga';
import { isUserAuthenticated } from './selectors/user.selector';
import { signOut } from './sagas/user.saga';
import { Alert } from './components/Alert';

const theme = createTheme({
  palette: {
    primary: {
      main: purple[500],
    },
    error: {
      main: red[500],
    }
  },
  components:{
    MuiAppBar: {
      styleOverrides: {
        colorPrimary: {
          color: 'white'
        }
      }
    }
  }
});

const ContentWrapper = styled(Stack)`
  width: 100%;
`;

function App() {
  const isAppLoaded = useSelector((state: IApplicationState) => isApplicationLoaded(state));
  const notifications = useSelector((state: IApplicationState) => state.notifications) || [];
  const isAuthenticated  = useSelector(isUserAuthenticated);

  const dispatch = useDispatch();
  const navigate = useNavigate();

  const onLoadingFail = useCallback(() => navigate('login'), [navigate]);

  const handleLogout = () => {
    dispatch(signOut());
  };

  useEffect(() => {
    if (!isAppLoaded) {
      dispatch(loading({onLoadingFail}));
    }
  }, [isAppLoaded, dispatch, onLoadingFail]);

  const defaultComponent = useMemo(() => {
    if(isAuthenticated) {
      return <Products />;
    }

    return <Login />;
  }, [isAuthenticated])

  const handleClose = (id: string) => () => dispatch(removeNotification(id));

  return (
    <ThemeProvider theme={theme}>
      <StyledThemeProvider theme={theme}>
        <Box sx={{ display: 'flex' }} className="App">
          <ContentWrapper>
            <Header
              isAuthenticated={isAuthenticated}
              title="Bike shop"
              onLogout={handleLogout}
              onLogin={() => navigate('/login')}
              goHome={() => navigate('/')}
            />
            <Stack spacing={0} direction="row">
                {notifications && notifications.map(el => <Snackbar key={el.id} open={true} autoHideDuration={5000} onClose={handleClose(el.id)}>
                  <Alert onClose={handleClose(el.id)} severity={el.type} sx={{ width: '100%' }}>
                    {el.message}
                  </Alert>
                </Snackbar>)}
                <Routes>
                  <Route path="/product" element={
                    <RequireAuth isAuthenticated={isAuthenticated}>
                      <Product />
                    </RequireAuth>
                  } />
                  <Route path="/product/:productId/edit" element={
                    <RequireAuth isAuthenticated={isAuthenticated}>
                      <Product />
                    </RequireAuth>
                  } />
                  <Route path="/products" element={
                    <RequireAuth isAuthenticated={isAuthenticated}>
                      <Products />
                    </RequireAuth>
                  } />
                  <Route path="/login" element={<Login />} />
                  <Route path="/register" element={<Register />} />
                  <Route path="*" element={defaultComponent}/>
                </Routes>
            </Stack>
          </ContentWrapper>
        </Box>
      </StyledThemeProvider>
    </ThemeProvider>
  );
}

export default App;
