import { useCallback, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { SubmitHandler } from "react-hook-form";
import { useNavigate, useParams } from "react-router-dom";

import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';

import { IProductFormData } from '../interfaces/product.interface';
import { createProduct, fetchProduct, resetProduct, editProduct } from '../sagas/product.saga';
import ProductForm from '../components/ProductForm';
import { ProductFormMode } from '../enums';
import { getProduct } from '../selectors/product.selector';
import { isModeEdit } from '../utils';

export const Product = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const { productId } = useParams();

  const product = useSelector(getProduct);
  const [mode, setMode] = useState(ProductFormMode.create);

  useEffect(() => {
    if(productId) {
      return setMode(ProductFormMode.edit);
    }

    return setMode(ProductFormMode.create);
  }, [productId])

  useEffect((): any => {
    if(!productId) return;
    
    dispatch(fetchProduct(productId));

    return () => dispatch(resetProduct());
  }, [productId, dispatch])

  const onSuccess = useCallback(() => navigate('/products'), [navigate]);

  const onSubmit: SubmitHandler<IProductFormData> = (data: IProductFormData) => {
    if(isModeEdit(mode)) {
      return dispatch(editProduct({...data, onSuccess}, productId as string));
    }
 
    return dispatch(createProduct({...data, onSuccess}));
  }

  return (
    <>
      <Grid sx={{ flexGrow: 1, mt: 3, '& .MuiTextField-root': { m: 1 } }} container justifyContent="center" spacing={6}>
        <Grid item xs={6}>
          <Box
            component="form"
            noValidate
            autoComplete="off"
          >
            <Typography variant="h4" component="h1" gutterBottom>
              Create new product
            </Typography>
            <ProductForm 
              onSend={onSubmit}
              mode={mode}
              product={product}
            />
          </Box>
        </Grid>
      </Grid>
    </>
  );
}
