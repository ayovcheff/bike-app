import { useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from "react-router-dom";

import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';

import { useForm, SubmitHandler } from "react-hook-form";
import { IUserCreationData } from '../interfaces/user.interface';
import { registration } from '../sagas/user.saga';
import { isUserSignedUp } from '../selectors/user.selector';
import { Button } from '@mui/material';
import Grid from '@mui/material/Grid';

export const Register = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const signUpSuccess = useSelector(isUserSignedUp);

  useEffect(() => {
    if (signUpSuccess) {
      navigate('/login')
    }
  }, [signUpSuccess, navigate]);

  const {
    register,
    handleSubmit,
    getValues,
    formState: { errors },
  } = useForm<IUserCreationData>();

  const onSubmit: SubmitHandler<IUserCreationData> = (data: IUserCreationData) => {
    dispatch(registration(data));
  }

  const matchesPreviousPassword = (value: string) => {
    const { password } = getValues();
    return password === value || "Passwords should match";
  };

  return (
    <Grid sx={{ flexGrow: 1, mt: 3, '& .MuiTextField-root': { m: 1, width: '30ch' } }} container justifyContent="center" spacing={6}>
      <Grid item xs={12}>
        <Box
          component="form"
          noValidate
          autoComplete="off"
        >
          <Typography variant="h4" gutterBottom component="div">
            Register
          </Typography>
          <div>
            <TextField
              label="Name"
              maxRows={4}
              { ...register('name', { required: 'Name is required' })}
            />
            <p style={{ color: "black" }}>{errors?.name?.message}</p>
          </div>
          <div>
            <TextField
              label="Email"
              maxRows={4}
              { ...register("email", { required: 'Email is required' })}
            />
            <p style={{ color: "black" }}>{errors?.email?.message}</p>
          </div>
          <div>
            <TextField
              label="Password"
              type="password"
              maxRows={4}
              {...register("password", { 
                required: 'Password is required',
                minLength: 6,
              })}
            />
            <p style={{ color: "black" }}>{errors?.password?.message}</p>
          </div>
          <div>
            <TextField
              label="Confirm password"
              type="password"
              maxRows={4}
              { ...register("passwordConfirmation", { 
                required: true, 
                minLength: 6,
                validate: {
                  matchesPreviousPassword,
                }
              })}
            />
              <p style={{ color: "black" }}>{errors?.passwordConfirmation?.message}</p>
          </div>
          <div>
            <Button variant="contained" onClick={handleSubmit(onSubmit)}>Sign up</Button>
            <p>If you have account</p>
            <Link to="/login">Sign in</Link>
          </div>
        </Box>
      </Grid>
    </Grid>
  );
}
