import {
  useLocation,
  Navigate,
} from "react-router-dom";
import { useSelector } from 'react-redux';
import { IApplicationState } from '../interfaces/application.interface';
import { isApplicationLoaded } from "../selectors/application.selectors";

interface IRequireAuth {
  isAuthenticated: boolean,
  children: JSX.Element,
}

export const RequireAuth = ({ isAuthenticated, children }: IRequireAuth) => {
  const isAuthenticationLoading = useSelector((state: IApplicationState) => state.user.isAuthenticationLoading);
  const isAppLoaded = useSelector((state: IApplicationState) => isApplicationLoaded(state));
  
  const location = useLocation();

  if (isAppLoaded && !isAuthenticated && !isAuthenticationLoading) {
    // Redirect them to the /login page, but save the current location they were
    // trying to go to when they were redirected. This allows us to send them
    // along to that page after they login, which is a nicer user experience
    // than dropping them off on the home page.
    return <Navigate to="/login" state={{ from: location }} replace />;
  }

  return children;
}