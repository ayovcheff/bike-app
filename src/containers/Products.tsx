import { useCallback } from 'react'
import styled from 'styled-components';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';

import { fetchAllProducts, deleteProduct } from '../sagas/product.saga';
import { getProducts } from '../selectors/product.selector';
import { useNavigate } from 'react-router-dom';
import ProductTable from '../components/ProductTable';

interface IStyledPaper {
  padding: number;
  theme: any;
} 

export const Wrapper = styled(Box)`
  width: 100%;
`;

const StyledPaper = styled(Paper)`
  boxShadow: 0px 0px 1px;
  padding: ${(props: IStyledPaper) => props.theme.spacing(props.padding)};
  margin: ${props => props.theme.spacing(2)};
  display: flex;
  flex-direction: row;
`;

export const Products = () => {
  const dispatch = useDispatch();
  const products = useSelector(getProducts);
  const navigate = useNavigate();

  useEffect(() => {
    dispatch(fetchAllProducts());
  }, [dispatch]);

  const onDelete = useCallback((id: string): void => {
    dispatch(deleteProduct(id));
  }, [dispatch]);
  
  const onEdit = useCallback((id: string): void => navigate(`/product/${id}/edit`), [navigate]);
  const onCreate = useCallback((): void => navigate('/product'), [navigate]);

  return (
    <Wrapper>
      <StyledPaper padding={0}>
        <ProductTable products={products} onDelete={onDelete} onEdit={onEdit} onCreate={onCreate}/>
      </StyledPaper>
    </Wrapper>
  );
}