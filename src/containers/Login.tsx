import { useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from "react-router-dom";

import Box from '@mui/material/Box';
import { Button, TextField } from '@mui/material';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import Alert from '@mui/material/Alert';

import { useForm, SubmitHandler } from "react-hook-form";

import { IApplicationState } from '../interfaces/application.interface';
import { REGISTRATION_SUCCESS } from '../constants/actions';
import { isUserAuthenticated, isUserSignedUp } from '../selectors/user.selector';
import { ILoginFormInput } from '../interfaces/user.interface';
import { signIn } from '../sagas/user.saga';

export const Login = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const {
    signUpSuccess,
    isAuthenticated,
  } = useSelector((state: IApplicationState) => ({
    isAuthenticated: isUserAuthenticated(state),
    signUpSuccess: isUserSignedUp(state),
  }));

  const { register, handleSubmit } = useForm<ILoginFormInput>();
  const onSubmit: SubmitHandler<ILoginFormInput> = (data) => {
    dispatch(signIn(data as ILoginFormInput));
  }

  useEffect(() => {
    if (isAuthenticated) {
      navigate('/products');
    }
  }, [isAuthenticated, navigate]);

  return (
    <Grid sx={{ flexGrow: 1, mt: 3, '& .MuiTextField-root': { m: 1, width: '30ch' } }} container justifyContent="center" spacing={6}>
      <Grid item xs={12}>
        <Box
          component="form"
          noValidate
          autoComplete="off"
        >
          {signUpSuccess && (<Alert severity="success">{REGISTRATION_SUCCESS}</Alert>)}
          <Typography variant="h4" gutterBottom component="div">
            Login
          </Typography>
          <div>
            <TextField
              { ...register("email")}
            />
          </div>
          <div>
            <TextField
              label="Password"
              type="password"
              { ...register("password")}
            />
          </div>
          <div>
            <Button 
              variant="contained" 
              size="medium"
              onClick={handleSubmit(onSubmit)}>Sign in</Button>
            <p>If you don't have account</p>
            <Link to="/register">Sign up</Link>
          </div>
        </Box>
      </Grid> 
    </Grid>
  );
}
