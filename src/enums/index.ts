export enum NotificationTypes {
  info = 'info',
  warning = 'warning',
  error = 'error',
  success = 'success',
}

export enum ProductFormMode {
  edit = 'edit',
  create = 'create',
}

export enum ProductColors {
  Yellow = 'Yellow',
  Black = 'Black',
  Green = 'Green',
  Red = 'Red',
  Blue = 'Blue',
}

export enum ProductSizes {
  M = 'M',
  L = 'L',
  XL = 'XL',
}