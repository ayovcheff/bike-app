import React from 'react';
import { DragDrop } from '@uppy/react';
import { isEmpty } from 'ramda';
import { v4 as uuidv4 } from 'uuid';

import Card from '@mui/material/Card';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';

import useUppyUpload from '../hooks/useUppy';
import { CenteredCardActions, StyledBadge, StyledStack } from './styles';

const ImageListComponent = ({images, onImageDelete}: any) => {
  return (
    <React.Fragment>
      { isEmpty(images) && (<p>Upload your first picture (*_-)</p>) }
      {!isEmpty(images) && (<StyledStack direction="row" spacing={2}>
        {images.map((image: any, index: number) => (
          <Card key={uuidv4()} sx={{ width: 150, position: 'relative' }}>
            {index === 0 && (<StyledBadge>Thumbnail</StyledBadge>)}
            <CardMedia
              component="img"
              image={image}
            />
            <CenteredCardActions>
              <Button size="small" variant="outlined" startIcon={<DeleteForeverIcon />} color="error" onClick={() => onImageDelete(image)}>
                Delete
              </Button>
            </CenteredCardActions>
          </Card>
        ))}
      </StyledStack>)}
    </React.Fragment>
  )
}


export const ImagePicker = ({ images, onSuccess, onFailure, onImageDelete }: any) => {
 const uppy = useUppyUpload('upload', onSuccess, onFailure);

  return (
    <React.Fragment>
      <ImageListComponent images={images} onImageDelete={onImageDelete} />
      <DragDrop
        uppy={uppy}
        locale={{
          strings: {
            // Text to show on the droppable area.
            // `%{browse}` is replaced with a link that opens the system file selection dialog.
            dropHereOr: 'Drop here or %{browse}',
            // Used as the label for the link that opens the system file selection dialog.
            browse: 'browse',
          },
        }}
      />
    </React.Fragment>
  )
};