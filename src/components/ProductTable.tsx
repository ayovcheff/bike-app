import { DataGrid, GridColDef, 
  GridToolbarContainer,
  GridToolbarFilterButton,
  GridToolbarExport,
} from '@mui/x-data-grid';
import IconButton from '@mui/material/IconButton';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import EditIcon from '@mui/icons-material/Edit';
import { IProduct } from '../interfaces/product.interface';
import Button from '@mui/material/Button';
import styled from 'styled-components';

interface IProductTableActions {
  onDelete: (id: string) => void,
  onEdit: (id: string) => void,
  onCreate: () => void,
  products: IProduct[],
}

const StyledToolbar = styled(GridToolbarContainer)`
  justify-content: space-between;
`;

const CustomToolbar = (onCreate: () => void) => () => {
  return (
    <StyledToolbar>
      <div>
        <GridToolbarExport />
        <GridToolbarFilterButton />
      </div>
      <div>
        <Button variant='outlined' onClick={onCreate}>Add</Button>
      </div>
    </StyledToolbar>
  );
}

const columns = ({onDelete, onEdit}: IProductTableActions): GridColDef[]  => [
  {
    field: 'images',
    headerName: 'Bike Image',
    width: 90,
    sortable: false,
    renderCell: (params) => {
      const {images} = params.row;
      return <img src={images[0]} alt='bike' loading="lazy" width="50" height="50"/>
    },
  },
  { field: 'model', headerName: 'Model',  width: 130},
  { field: 'description', headerName: 'Description',  flex: 1},
  { field: 'color', headerName: 'Color', width: 130 },
  { field: 'size', headerName: 'Size', width: 90},
  {
    field: "edit",
    headerName: "Edit",
    sortable: false,
    renderCell: (params) => {
      const {id} = params.row;
      return <IconButton
        size="small"
        edge="start"
        color="inherit"
        aria-label="open drawer"
        sx={{ mr: 2 }}
        onClick={() => onEdit(id)}
      >
        <EditIcon />
      </IconButton>;
    },
    width: 90,
  },

  {
    field: "delete",
    headerName: "Delete",
    sortable: false,
    renderCell: (params) => {
      const {id} = params.row;
      return <IconButton
        size="small"
        edge="start"
        color="inherit"
        aria-label="open drawer"
        sx={{ mr: 2 }}
        onClick={() => onDelete(id)}
      >
        <DeleteForeverIcon />
      </IconButton>; 
    },
    width: 90,
  },
];

export default function ProductTable(params: IProductTableActions) {
  return (
    <div style={{ height: 400, width: '100%' }}>
      <DataGrid
        rows={params.products}
        columns={columns(params)}
        components={{ Toolbar: CustomToolbar(params.onCreate) }}
        rowsPerPageOptions={[5]}
        pageSize={5}
      />
    </div>
  );
}