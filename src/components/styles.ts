import styled from 'styled-components';
import Box from '@mui/material/Box';
import Stack from '@mui/material/Stack';
import CardActions from '@mui/material/CardActions';

export const StyledBox = styled(Box)`
  display: flex;
  flex-direction: column;

  .MuiTextField-root {
    width: 100%;
  }
`;

export const StyledBoxFooter = styled(StyledBox)`
  flex-direction: row;
  justify-content: center;

  button {
    margin-right: 10px;
  }
`

export const ErrorMsg = styled.p`
  color: ${props => props.theme.palette.error.main};
  margin-top: 0;
  font-size: 14px;
`;

export const StyledStack = styled(Stack)`
  padding: ${props => props.theme.spacing(2)};
`

export const StyledBadge = styled.div`
  background: ${props => props.theme.palette.primary.main};
  position: absolute;
  top: 16px;
  left: 54px;
  box-shadow: 0px 1px 1px rgba(0, 0, 0, 0.2);
  transform: translate3d(0, 0, 1px) rotate(45deg);
  backface-visibility: hidden;
  color: #fff;
  font-weight: bold;
  font-size: 12px;
  padding: 6px;
  z-index: 3;
  width: 120px;
  text-align: center;
  margin: auto;
`;

export const CenteredCardActions = styled(CardActions)`
  justify-content: center;
`;