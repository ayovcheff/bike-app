import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import LoginIcon from '@mui/icons-material/Login';
import LogoutIcon from '@mui/icons-material/Logout';
import ElectricBikeIcon from '@mui/icons-material/ElectricBike';
import { Button } from '@mui/material';
import styled from 'styled-components';

interface IHeader {
  isAuthenticated: boolean,
  title: string,
  onLogin: () => void,
  onLogout: () => void,
  goHome: () => void,
}

const Logo = styled(Button)`
  font-weight: bold;
`;

export const Header = ({onLogin, onLogout, goHome, isAuthenticated, title}: IHeader) => {

  return (
      <AppBar position="static">
        <Toolbar>
          <Logo
            color="inherit"
            variant="text"
            onClick={goHome}
            startIcon={<ElectricBikeIcon />}>
            {title}
          </Logo>
          <Box flexGrow={1} display="flex">
          </Box>
          <Box display="flex">
            {!isAuthenticated && <IconButton
              size="large"
              edge="start"
              color="inherit"
              aria-label="login"
              onClick={onLogin}
            >
              <LoginIcon />
            </IconButton>}
            {isAuthenticated &&
            <IconButton
              size="large"
              edge="start"
              color="inherit"
              aria-label="login"
              onClick={onLogout}
            >
              <LogoutIcon />
            </IconButton>}
          </Box>
        </Toolbar>
      </AppBar>
  );
}
