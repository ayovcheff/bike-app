import { useCallback, useEffect, useMemo, useState } from 'react';
import { useForm } from "react-hook-form";
import {curry, isEmpty, without } from 'ramda';

import { ErrorMessage } from '@hookform/error-message';
import LinearProgress from '@mui/material/LinearProgress';
import TextField from '@mui/material/TextField';
import Fade from '@mui/material/Fade';
import Button from '@mui/material/Button';
import InputAdornment from '@mui/material/InputAdornment';

import MenuItem from '@mui/material/MenuItem';

import { StyledBox, StyledBoxFooter, ErrorMsg } from './styles';
import { IProduct, IProductFormData } from '../interfaces/product.interface';
import { ImagePicker } from './ImagePicker';
import { Either } from 'lambda-ts';
import { ProductFormMode, ProductColors, ProductSizes } from '../enums';
import { isModeEdit } from '../utils';

interface IProductFormProps {
  onSend: (data: IProductFormData) => void,
  mode: ProductFormMode,
  product: IProduct,
}

export default function ProductForm({ onSend, product, mode }: IProductFormProps) {
  const [loading, setLoading] = useState(false);
  const [images, setImages] = useState(product.images);

  const { 
    register,
    handleSubmit,
    setValue,
    formState: { errors },
    setError,
    getValues,
    trigger,
  } = useForm({
    criteriaMode: 'all',
    mode: "onChange"
  });

  useEffect(() => {
    register('images', {required: 'You should upload at least one image' });
  }, [register]);

  useEffect(() => {
    // onli if mode is edit fields should be populated
    if (product && isModeEdit(mode)) {
      setValue('model', product.model, { shouldValidate: true });
      setValue('description', product.description, { shouldValidate: true });
      console.log('product size', product.size);
      setValue('size', product.size, { shouldValidate: true });
      setValue('price', product.price, { shouldValidate: true });
      setValue('images', product.images, { shouldValidate: true });
      setImages(product.images);
    }
  }, [product, mode, setValue]);

  const onSubmit = useCallback(async (data) => {
    await trigger();
    if(!isEmpty(errors)) {
      return;
    }

    setLoading(true);
    onSend(data);
  }, [onSend, errors, trigger]);

  // Add Array value data
  const addElement = curry((fieldName: string, newElements: string[]): string[] => {
    const result = Either(getValues(fieldName))
      .fold(() => [...newElements], (curentImages: string[]) => [...curentImages, ...newElements])

    return result || [];
  });

  const handleImageDelete = (img: string): void => {
    const updatedImages = Either(images)
      .map(images => without([img], images))
      .getOrElse([] as string[]);

    setImages(updatedImages as string[]);
    setValue('images', []);
  }

  const handleUpdateArrayField = curry((fieldName: string, successCb: (v: string[]) => void, { uploadURL }: Record<string, string>) => {
    const currentElms = addElement(fieldName, [uploadURL]);
    setValue(fieldName, currentElms , { shouldValidate: true });
    
    Either(!errors[fieldName]).map(() => successCb(currentElms));
  });

  const handleUploadFailure = (message: string) => {
    setError('images', { type: 'manual', message });
  }

  const shrink = true;
  const colors = useMemo(() => Object.keys(ProductColors), []);
  const sizes = useMemo(() => Object.keys(ProductSizes), []);
  const submitButtonLabel = useMemo(() => isModeEdit(mode) ? 'Edit' : 'Create', [mode]);

  return (
    <>
      <StyledBox>
        <div>
          <Fade
            in={loading}
            style={{
              transitionDelay: loading ? '200ms' : '0ms',
              verticalAlign: 'middle',
            }}
            unmountOnExit
          >
            <LinearProgress />
          </Fade>
        </div>
        <div>
          <TextField
            label="Model"
            fullWidth
            defaultValue={product.model}
            InputLabelProps={{ shrink }}
            { ...register('model', { required: 'Model is required' })}
          />
          <ErrorMessage errors={errors} name="model" as={ErrorMsg}/>
        </div>
        <div>
          <TextField
            label="Description"
            multiline
            fullWidth
            defaultValue={product.description}
            placeholder="Tell me something about the product"
            InputLabelProps={{ shrink }}
            { ...register('description', { required: 'Description is required' })}
          />
          <ErrorMessage errors={errors} name="description" as={ErrorMsg} />
        </div>
        <div>
          <TextField
            select
            fullWidth
            label="Color"
            defaultValue={product.color || colors[0]}
            inputProps={register('color', {
              required: 'Please enter currency',
            })}
          >
            {colors.map((value) => (
              <MenuItem key={value} value={value}>
                {value}
              </MenuItem>
            ))}
          </TextField>
          <ErrorMessage errors={errors} name="color" as={ErrorMsg} />
        </div>

        <div>
          <TextField
            select
            fullWidth
            label="Size"
            defaultValue={product.size || sizes[0]}
            inputProps={register('size', {
              required: 'Please enter currency',
            })}
          >
            {sizes.map((value) => (
              <MenuItem key={value} value={value}>
                {value}
              </MenuItem>
            ))}
          </TextField>
          <ErrorMessage errors={errors} name="size" as={ErrorMsg} />
        </div>
        <div>
          <ImagePicker
            images={images}
            onSuccess={handleUpdateArrayField('images', setImages)}
            onFailure={handleUploadFailure}
            onImageDelete={handleImageDelete} />
          <ErrorMessage errors={errors} name="images" as={ErrorMsg} />
        </div>
        <div>
          <TextField
            label="Price"
            type="number"
            min={100}
            fullWidth
            defaultValue={product.price}
            InputProps={{
              endAdornment: <InputAdornment position="start">EUR</InputAdornment>,
            }}
            InputLabelProps={{ shrink }}
            {...register("price", {
              valueAsNumber: true,
            })}
          />
          <ErrorMessage errors={errors} name="price" as={ErrorMsg}/>
        </div>
      </StyledBox>
      <StyledBoxFooter >
          <Button variant="contained" onClick={handleSubmit(onSubmit)}>{submitButtonLabel}</Button>
      </StyledBoxFooter>
    </>
  );
}
