export interface IUserCreationData {
  email: string;
  password: string;
  passwordConfirmation: string;
  name: string;
};

export interface IUserState {
  name: string;
  email: string;
  signUpSuccess: boolean;
  isAuthenticated: boolean;
  isAuthenticationLoading: boolean;
}

export interface ILoginFormInput {
  email: String;
  password: String;
}
