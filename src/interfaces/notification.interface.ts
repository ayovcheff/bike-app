import { NotificationTypes } from "../enums";

export interface INotification {
  type: NotificationTypes,
  message: string,
  id: string,
};