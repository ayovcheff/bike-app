export type IAction = {
  payload: any,
  type: string,
}

export interface ActionOptions {
  request: (payload?: any) => IAction,
  success: (payload?: any) => IAction,
  failure: (payload?: any) => IAction,
}