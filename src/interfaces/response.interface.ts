export interface IResponse {
  json: any;
  headers: Headers;
};

export interface ResponseGenerator{
  config?:any,
  data?:any,
  headers?:any,
  request?:any,
  status?:number,
  statusText?:string
}