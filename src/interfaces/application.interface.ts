import { INotification } from "./notification.interface";
import { IProductState } from "./product.interface";
import { IUserState } from "./user.interface";

export interface IApplication {
  isAppLoaded: boolean,
}

export interface IApplicationState {
  user: IUserState,
  product: IProductState,
  notifications: INotification[],
}