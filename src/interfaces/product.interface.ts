export interface IProduct {
  model: string;
  description: string;
  price: number;
  createdAt: string,
  images: string[],
  color: string,
  size: string,
  id?: string;
};

export interface IProductState extends IProduct{
};

export interface IProductFormData extends IProduct {
  // images: string[],
  onSuccess: () => void,
}