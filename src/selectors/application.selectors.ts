import {
  pathOr,
} from 'ramda';
import { IApplicationState } from '../interfaces/application.interface';

export const isApplicationLoaded = (state: IApplicationState) => pathOr(false, ['application', 'isAppLoaded'], state);