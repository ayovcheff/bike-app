import {
  pathOr,
} from 'ramda';
import { IApplicationState } from '../interfaces/application.interface';
import { IProduct } from '../interfaces/product.interface';

export const getProducts = (state: IApplicationState): IProduct[] => pathOr([], ['products'], state);
export const getProduct = (state: IApplicationState): IProduct => pathOr({} as IProduct, ['product'], state);
