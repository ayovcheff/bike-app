import {
    pathOr,
} from 'ramda';
import { IApplicationState } from '../interfaces/application.interface';

export const isUserAuthenticated = (state: IApplicationState) => pathOr(false, ['user', 'isAuthenticated'], state);
export const isUserSignedUp = (state: IApplicationState) => pathOr(false, ['user', 'signUpSuccess'], state);
export const getUserId = (state: IApplicationState) => pathOr('', ['user', 'id'], state);
export const getUserEmail = (state: IApplicationState) => pathOr('', ['user', 'email'], state);
export const getUsername = (state: IApplicationState) => pathOr('', ['user', 'name'], state);