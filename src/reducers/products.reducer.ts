import { equals } from 'ramda';
import {
  deleteProductActions,
  fetchAllProductsActions,
} from '../constants/actions';
import { IAction } from '../interfaces/action.interface';
import { IProductState } from '../interfaces/product.interface';

const initialState: IProductState[] = [];

/* Reducers */
// eslint-disable-next-line import/no-anonymous-default-export
export default (state = initialState, action: IAction) => {
  switch(action.type) {
    case (fetchAllProductsActions.success().type): return ([
      ...action.payload,
    ]);

    case(deleteProductActions.success().type): return ([
      ...state.filter(p => !equals(p.id, action.payload.productId)),
    ]);

    default:
      return [
        ...state,
      ]
  };
};
