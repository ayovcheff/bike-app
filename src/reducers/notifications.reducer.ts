import { equals } from 'ramda';
import { ADD_NOTIFICATION, REMOVE_NOTIFICATION } from '../constants/actions/notification.constants';

import { IAction } from '../interfaces/action.interface';
import { INotification } from '../interfaces/notification.interface';


const initialState: INotification[] = [];

// eslint-disable-next-line import/no-anonymous-default-export
export default (state = initialState, action: IAction) => {
  switch(action.type) {
    case (ADD_NOTIFICATION): return ([
      ...state,
      action.payload,
    ]);

    case (REMOVE_NOTIFICATION): return state.reduce((acc, el) => {
      if(equals(action.payload.id, el.id)) return acc;

      return [...acc, el];
    }, [] as INotification[]);

    default:
      return [
        ...state,
      ]
  };
};