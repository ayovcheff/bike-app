import { combineReducers } from "redux";
import productReducer from './product.reducer';
import productsReducer from './products.reducer';
import userReducer from "./user.reducer";
import notificationsReducer from './notifications.reducer';
import applicationReducer from './application.reducer';

const rootReducer = combineReducers({
  products: productsReducer,
  product: productReducer,
  user: userReducer,
  notifications: notificationsReducer,
  application: applicationReducer,
});

export type RootState = ReturnType<typeof rootReducer>;

export default rootReducer;