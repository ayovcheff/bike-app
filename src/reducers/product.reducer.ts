import {
  fetchProductActions, resetProductActions,
} from '../constants/actions';
import { ProductColors, ProductSizes } from '../enums';
import { IAction } from '../interfaces/action.interface';
import { IProductState } from '../interfaces/product.interface';

const initialState: IProductState = {
  model: '',
  description: '',
  color: ProductColors.Red,
  size: ProductSizes.M,
  price: 0,
  createdAt: Date.now().toString(),
  images: [],
};

/* Reducers */
// eslint-disable-next-line import/no-anonymous-default-export
export default (state = initialState, action: IAction) => {
  switch(action.type) {
    case (fetchProductActions.success().type): return ({
      ...state,
      ...action.payload,
    });

    case (resetProductActions.success().type): return ({
      ...initialState,
    });

    default:
      return {
        ...state,
      }
  };
};
