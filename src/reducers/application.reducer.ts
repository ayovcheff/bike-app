import { applicationLoadActions } from '../constants/actions';
import { IAction } from '../interfaces/action.interface';
import { IApplication } from '../interfaces/application.interface';

const initialState: IApplication = {
  isAppLoaded: false,
};

/* Reducers */
// eslint-disable-next-line import/no-anonymous-default-export
export default (state = initialState, action: IAction) => {
  switch(action.type) {
    case (applicationLoadActions.success().type): return ({
      ...state,
      isAppLoaded: true,
    });

    default:
      return {
        ...state,
      }
  };
};