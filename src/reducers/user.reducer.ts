// import { cond, equals, T } from 'ramda';
import {
  fetchUserActions,
  loginActions,
  createUserActions,
  extractUser,
  logoutActions,
} from '../constants/actions';
import { IAction } from '../interfaces/action.interface';
import { IUserState } from '../interfaces/user.interface';

const initialState: IUserState = {
  name: '',
  email: '',
  signUpSuccess: false,
  isAuthenticated: false,
  isAuthenticationLoading: false,
};

/* Reducers */
// eslint-disable-next-line import/no-anonymous-default-export
export default (state = initialState, action: IAction) => {
  switch(action.type) {
    case (fetchUserActions.success().type): return ({
      ...state,
      showHelpDialog: true,
    });

    case(createUserActions.success().type): return ({
      ...state,
      signUpSuccess: true,
    });

    case(loginActions.request().type): return ({
      ...state, 
      isAuthenticationLoading: true,
    });

    case(loginActions.success().type): return ({
      ...state,
      ...action.payload,  
      isAuthenticated: true,
      isAuthenticationLoading: false,
    });

    case(logoutActions.success().type): return ({
      ...state, 
      isAuthenticated: false,
    });

    case(loginActions.failure().type): return ({
      ...state, 
      isAuthenticationLoading: false,
    });

    case(extractUser.request().type): return ({
      ...state,  
      isAuthenticationLoading: true,
    });

    case(extractUser.success().type): return ({
      ...state,
      ...action.payload,  
      isAuthenticated: true,
      isAuthenticationLoading: false,
    });

    case(extractUser.failure().type): return ({
      ...initialState, 
      isAuthenticated: false,
      isAuthenticationLoading: false,
    });

    default:
      return {
        ...state,
      }
  };
};
