import { equals } from "ramda";
import { ProductFormMode } from "../enums";
import { ActionOptions } from "../interfaces/action.interface";

export const createActionNames = (name: string): ActionOptions => ({
  request: (payload: any) => ({ payload, type: `${name}_REQUEST`}),
  success: (payload: any) => ({ payload, type:`${name}_SUCCESS`}),
  failure: (payload: any) => ({ payload, type: `${name}_FAILURE` }),
});

// Util for validation if Product form mode is edit
export const isModeEdit = equals(ProductFormMode.edit);